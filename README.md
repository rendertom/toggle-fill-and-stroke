![Toggle Fill and Stroke](/Toggle%20Fill%20and%20Stroke.png)

# Toggle Fill and Stroke #
Script for After Effects to morph any shape into a circle.

### Installation: ###
Clone or download this repository and copy **Toggle Fill and Stroke.jsx** to ScriptUI Panels folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking Window -> **Toggle Fill and Stroke**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------