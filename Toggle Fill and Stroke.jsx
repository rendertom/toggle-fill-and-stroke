/**********************************************************************************************
    Toggle Fill and Stroke visibility.
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
    	Toggles Fill or Stroke visibility for selected Shape Layers in a composition.
    	If no layers selected, then applies to all layers in active composition.

	Change log:
		v1.0 - 2017 06 30
			- Initial release
**********************************************************************************************/

(function (thisObj) {
	scriptBuildUI(thisObj);

	function scriptBuildUI(thisObj) {
		var win = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Toggle Fill and Stroke", undefined, {
			resizeable: true
		});

		win.alignChildren = ["fill", "fill"];
		win.spacing = 5;

		win.fillControls = win.add("group");
		win.fillControls.alignChildren = ["fill", "fill"];
		win.btnEnableFills = win.fillControls.add("button", undefined, "Enable Fills");
		win.btnDisableFills = win.fillControls.add("button", undefined, "Disable Fills");

		win.strokeControls = win.add("group");
		win.strokeControls.alignChildren = ["fill", "fill"];
		win.btnEnableStrokes = win.strokeControls.add("button", undefined, "Enable Strokes");
		win.btnDisableStrokes = win.strokeControls.add("button", undefined, "Disable Strokes");

		win.btnEnableFills.targetMatchName = win.btnDisableFills.targetMatchName = "ADBE Vector Graphic - Fill";
		win.btnEnableStrokes.targetMatchName = win.btnDisableStrokes.targetMatchName = "ADBE Vector Graphic - Stroke";
		win.btnEnableFills.setValue = win.btnEnableStrokes.setValue = true;
		win.btnDisableFills.setValue = win.btnDisableStrokes.setValue = false;

		win.btnEnableFills.onClick = win.btnDisableFills.onClick =
			win.btnEnableStrokes.onClick = win.btnDisableStrokes.onClick = function () {
				main(this.targetMatchName, this.setValue);
			};

		win.onResizing = win.onResize = function () {
			this.layout.resize();
		};

		if (win instanceof Window) {
			win.center();
			win.show();
		} else {
			win.layout.layout(true);
			win.layout.resize();
		}
	}

	function main(targetMatchName, setValue) {
		var composition = app.project.activeItem;
		if (!composition || !(composition instanceof CompItem))
			return alert("Please select composition first");

		var selectedLayers = getSelectedLayers(composition);

		app.beginUndoGroup("Toggle Visibility");
		for (var i = 0, il = selectedLayers.length; i < il; i++) {
			setPropertyValue(selectedLayers[i], targetMatchName, setValue);
		}
		app.endUndoGroup();
	}

	function getSelectedLayers(composition) {
		var selectedLayers = composition.selectedLayers;
		if (selectedLayers.length === 0) {
			for (var i = 1, il = composition.numLayers; i <= il; i++) {
				selectedLayers.push(composition.layer(i));
			}
		}
		return selectedLayers;
	}

	function setPropertyValue(currentProperty, targetMatchName, value) {
		for (var i = 1, il = currentProperty.numProperties; i <= il; i++) {
			if (currentProperty.property(i).matchName === targetMatchName)
				currentProperty.property(i).enabled = value;

			setPropertyValue(currentProperty.property(i), targetMatchName, value);
		}
	}
})(this);